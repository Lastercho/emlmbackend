package net.is_bg.emlm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmlmApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmlmApplication.class, args);
    }

}
