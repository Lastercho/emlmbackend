package net.is_bg.emlm.model.entity;

import net.is_bg.emlm.model.entity.system.User;
import org.springframework.boot.context.properties.bind.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Objects;


@Entity(name = "applications")
public class Application extends BaseEntityWithTime {

    @Column(unique = true)
    private String applicationName;
    private String backReference;
    @OneToMany
    private List<User> userId;
    private String secret;

    public Application() {
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getBackReference() {
        return backReference;
    }

    public void setBackReference(String backReference) {
        this.backReference = backReference;
    }

    public List<User> getUserId() {
        return userId;
    }

    public void setUserId(List<User> userId) {
        this.userId = userId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Application that = (Application) o;
        return Objects.equals(applicationName, that.applicationName) && Objects.equals(backReference, that.backReference) && Objects.equals(userId, that.userId) && Objects.equals(secret, that.secret);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applicationName, backReference, userId, secret);
    }
}