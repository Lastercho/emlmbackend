package net.is_bg.emlm.model.entity.system;

import net.is_bg.emlm.model.entity.ApplicationGroup;
import net.is_bg.emlm.model.entity.ApplicationRole;
import net.is_bg.emlm.model.entity.BaseEntityWithTime;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users", schema = "system")
public class User extends BaseEntityWithTime {

    @Column(columnDefinition = "TEXT",unique = true,nullable = false)
    private String email;
    @Column(length = 20,nullable = false,unique = true)
    private String personIdn;
    @Column(length = 200)
    private String personName;
    @Column(length = 200)
    private String employeePosition;
    @Column(columnDefinition = "TEXT",nullable = false)
    private String password;
    @Column(length = 16)
    private String refreshToken;
    private LocalDateTime lastPasswordChange;
    @ManyToOne
    private Group groupId;
    @Column(columnDefinition = "TEXT")
    private String lastAccessToken;
    @OneToMany
    private List<Role> role_id;
    @ManyToMany
    private List<ApplicationRole> applicationRoles;
    @ManyToMany
    private List<ApplicationGroup> applicationGroups;


    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonIdn() {
        return personIdn;
    }

    public void setPersonIdn(String personIdn) {
        this.personIdn = personIdn;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getEmployeePosition() {
        return employeePosition;
    }

    public void setEmployeePosition(String employeePosition) {
        this.employeePosition = employeePosition;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public LocalDateTime getLastPasswordChange() {
        return lastPasswordChange;
    }

    public void setLastPasswordChange(LocalDateTime lastPasswordChange) {
        this.lastPasswordChange = lastPasswordChange;
    }

    public Group getGroupId() {
        return groupId;
    }

    public void setGroupId(Group groupId) {
        this.groupId = groupId;
    }

    public String getLastAccessToken() {
        return lastAccessToken;
    }

    public void setLastAccessToken(String lastAccessToken) {
        this.lastAccessToken = lastAccessToken;
    }

    public List<Role> getRole_id() {
        return role_id;
    }

    public void setRole_id(List<Role> role_id) {
        this.role_id = role_id;
    }

    public List<ApplicationRole> getApplicationRoles() {
        return applicationRoles;
    }

    public void setApplicationRoles(List<ApplicationRole> applicationRoles) {
        this.applicationRoles = applicationRoles;
    }

    public List<ApplicationGroup> getApplicationGroups() {
        return applicationGroups;
    }

    public void setApplicationGroups(List<ApplicationGroup> applicationGroups) {
        this.applicationGroups = applicationGroups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email) && Objects.equals(personIdn, user.personIdn) && Objects.equals(personName, user.personName) && Objects.equals(employeePosition, user.employeePosition) && Objects.equals(password, user.password) && Objects.equals(refreshToken, user.refreshToken) && Objects.equals(lastPasswordChange, user.lastPasswordChange) && Objects.equals(groupId, user.groupId) && Objects.equals(lastAccessToken, user.lastAccessToken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, personIdn, personName, employeePosition, password, refreshToken, lastPasswordChange, groupId, lastAccessToken);
    }
}

