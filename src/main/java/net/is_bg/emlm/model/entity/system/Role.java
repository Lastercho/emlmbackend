package net.is_bg.emlm.model.entity.system;

import net.is_bg.emlm.model.entity.BaseEntityWithTime;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name = "roles", schema = "system")
public class Role extends BaseEntityWithTime {

    private String roleName;
    @OneToOne
    private Role parentRoleId;

    public Role() {
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Role getParentRoleId() {
        return parentRoleId;
    }

    public void setParentRoleId(Role parentRoleId) {
        this.parentRoleId = parentRoleId;
    }
}
