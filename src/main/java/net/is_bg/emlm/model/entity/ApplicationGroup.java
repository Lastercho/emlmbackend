package net.is_bg.emlm.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity(name = "application_groups")
public class ApplicationGroup extends BaseEntity{

    @ManyToOne
    private Application applicationId;
    @Column(columnDefinition = "TEXT")
    private String groupName;
    @Column(columnDefinition = "TEXT")
    private String groupCode;
    @Column(columnDefinition = "TEXT")
    private String description;

    public ApplicationGroup() {
    }

    public Application getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Application applicationId) {
        this.applicationId = applicationId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
