package net.is_bg.emlm.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity(name = "application_role")
public class ApplicationRole extends BaseEntity{

    @ManyToOne
    private Application applicationId;
    @Column(columnDefinition = "TEXT")
    private String roleName;
    @Column(columnDefinition = "TEXT")
    private String roleCode;
    @Column(columnDefinition = "TEXT")
    private String description;

    public ApplicationRole() {
    }

    public Application getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Application applicationId) {
        this.applicationId = applicationId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
