package net.is_bg.emlm.model.entity.system;

import net.is_bg.emlm.model.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "groups", schema = "system")
public class Group extends BaseEntity {

    private String groupName;
    private String description;

    public Group() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
