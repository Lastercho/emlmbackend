package net.is_bg.emlm.service;

import net.is_bg.emlm.exception.UseNotFoundException;
import net.is_bg.emlm.model.entity.system.User;
import net.is_bg.emlm.repository.system.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User findUserByName(String name) {
        return userRepository.findUserByName(name).orElseThrow(() -> new UseNotFoundException("User with name " + name + " was not found."));
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findUserById(id).orElseThrow(() -> new UseNotFoundException("User with id " + id + " was not found."));
    }
}
