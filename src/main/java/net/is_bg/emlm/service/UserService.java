package net.is_bg.emlm.service;

import net.is_bg.emlm.model.entity.system.User;

import java.util.List;

public interface UserService {
    List<User> findAllUser();

    User findUserByName(String name);

    User findUserById(Long id);
}
