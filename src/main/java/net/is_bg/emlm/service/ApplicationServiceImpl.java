package net.is_bg.emlm.service;

import net.is_bg.emlm.exception.UseNotFoundException;
import net.is_bg.emlm.model.entity.Application;
import net.is_bg.emlm.repository.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    private final ApplicationRepository applicationRepository;

    @Autowired
    public ApplicationServiceImpl(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }


    public Application addApplication(Application application){
        return applicationRepository.save(application);
    }

    public Application findApplicationByName(String name){
        return applicationRepository.getApplicationByApplicationName(name).orElseThrow(() -> new UseNotFoundException("Application with name " + name + " was not found."));
    }

    @Override
    public Application findApplicationById(Long id) {
        return applicationRepository.getApplicationById(id).orElseThrow(() -> new UseNotFoundException("Application with id " + id + " was not found."));
    }

    @Override
    public boolean areImported() {
        return false;
    }

    @Override
    public String readApplicationFileContent() throws IOException {
        return null;
    }

    @Override
    public String importApplication() throws JAXBException, FileNotFoundException {
        return null;
    }

    @Override
    public List<Application> findAllApplication() {
        return applicationRepository.findAll() ;
    }
}
