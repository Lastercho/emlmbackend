package net.is_bg.emlm.service;

import net.is_bg.emlm.model.entity.Application;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface ApplicationService {

    boolean areImported();

    String readApplicationFileContent() throws IOException;

    String importApplication() throws JAXBException, FileNotFoundException;

    List<Application> findAllApplication();

    Application findApplicationByName(String name);

    Application findApplicationById(Long id);
}
