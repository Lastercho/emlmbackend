package net.is_bg.emlm.web.controllers;


import net.is_bg.emlm.model.entity.User;
import net.is_bg.emlm.model.entity.system.User;
import net.is_bg.emlm.repository.system.UserRepository;
import net.is_bg.emlm.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Emlm/api/user")
public class UserController extends BaseController {

    private final UserService userService;
    private final UserRepository userRepository;

    public UserController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }


    @GetMapping("/")
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> users = UserService.findAllUser();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/findname/{name}")
    public ResponseEntity<User> getUserByName(@PathVariable("name") String name){
        User user = UserService.findUserByName(name);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
    @GetMapping("/find/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") Long id){
        User user = UserService.findUserById(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping(path = "/add/{user}")
    public ResponseEntity<User> setUser(@PathVariable("user") User user){
        return null;
    }

}
