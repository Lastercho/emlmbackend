package net.is_bg.emlm.web.controllers;


import net.is_bg.emlm.model.entity.Application;
import net.is_bg.emlm.repository.ApplicationRepository;
import net.is_bg.emlm.service.ApplicationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Emlm/api/application")
public class ApplicationController extends BaseController {

    private final ApplicationService applicationService;
    private final ApplicationRepository applicationRepository;

    public ApplicationController(ApplicationService applicationServiceService, ApplicationRepository applicationRepository) {
        this.applicationService = applicationServiceService;
        this.applicationRepository = applicationRepository;
    }


    @GetMapping("/")
    public ResponseEntity<List<Application>> getAllApplications(){
        List<Application> applications = applicationService.findAllApplication();
        return new ResponseEntity<>(applications, HttpStatus.OK);
    }

    @GetMapping("/findname/{name}")
    public ResponseEntity<Application> getApplicationByName(@PathVariable("name") String name){
        Application application = applicationService.findApplicationByName(name);
        return new ResponseEntity<>(application, HttpStatus.OK);
    }
    @GetMapping("/find/{id}")
    public ResponseEntity<Application> getApplicationById(@PathVariable("id") Long id){
        Application application = applicationService.findApplicationById(id);
        return new ResponseEntity<>(application, HttpStatus.OK);
    }

    @PutMapping(path = "/add/{application}")
    public ResponseEntity<Application> setApplication(@PathVariable("application") Application application){
        return null;
    }

}
