package net.is_bg.emlm.repository;

import net.is_bg.emlm.model.entity.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
    Optional<Application> getApplicationByApplicationName(String name);
    Optional<Application> getApplicationById(Long id);
}
