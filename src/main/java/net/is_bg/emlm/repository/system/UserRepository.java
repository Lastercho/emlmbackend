package net.is_bg.emlm.repository.system;

import net.is_bg.emlm.model.entity.system.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserById(Long id);

    Optional<User> findUserByName(String name);
}
