package net.is_bg.emlm.repository.system;

import net.is_bg.emlm.model.entity.system.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Long> {
}
