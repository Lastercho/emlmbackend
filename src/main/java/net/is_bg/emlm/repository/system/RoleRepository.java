package net.is_bg.emlm.repository.system;

import net.is_bg.emlm.model.entity.system.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
