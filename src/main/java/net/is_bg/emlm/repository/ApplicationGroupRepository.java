package net.is_bg.emlm.repository;


import net.is_bg.emlm.model.entity.ApplicationGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationGroupRepository extends JpaRepository<ApplicationGroup, Long> {

}
